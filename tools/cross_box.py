import torch as t
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import os
import glob
import re
import shutil
import random

#load images
images_path = 'dataset/original_images'
crosses_path = 'datasets/crosses'

images = glob.glob(os.path.join(images_path, '*.jpg'))
crosses = glob.glob(os.path.join(crosses_path, '*.npy'))

images.sort(key=lambda f: int(re.sub('\D', '', f)))
crosses.sort(key=lambda f: int(re.sub('\D', '', f)))

#plot images
def plot_images(images):
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(plt.imread(images[1]))
    x,y = np.load(crosses[1])
    ax.scatter(x, y, c='r', s=10)
    plt.show()

#create boxes
for i, c in enumerate(crosses):
    x,y = np.load(c)
    x = x/500.0
    y = y/500.0
    #calculate center, width and height
    center = [(x.max()-x.min())/2+x.min(), (y.max()-y.min())/2+y.min()]
    width = x.max()-x.min()
    height = y.max()-y.min()
    box = [0, center[0], center[1], width, height]
    #round to 6 decimal places
    box = [round(x, 6) for x in box]
    #save boxes in txt file with the same name as image
    with open(f'datasets/labels/{i+1}.txt', 'w') as f:
        f.write(' '.join(str(x) for x in box))
        f.write('\n')

#upload boxes from file
boxes = glob.glob('datasets/labels/*.txt')
boxes.sort(key=lambda f: int(re.sub('\D', '', f)))
#plot boxes and images
fig, ax = plt.subplots(figsize=(10, 10))
ax.imshow(plt.imread(images[0]))
x,y = np.load(crosses[0])
ax.scatter(x, y, c='r', s=0.1)
with open(boxes[0], 'r') as f:
    box = f.read().split()
    box = [float(x) for x in box]
    print(box)
    x_center = box[1]*500.0
    y_center = box[2]*500.0
    width = box[3]*500.0
    height = box[4]*500
    print(x,y,width,height)
    rect = plt.Rectangle((x_center-width/2, y_center-height/2), width, height, fill=False, color='r')
    ax.add_patch(rect)
plt.show()

#split the code in train, val and test
#randomply get 20 images from dataset and move them to test folder
images = glob.glob('datasets/images/*.jpg')
images.sort(key=lambda f: int(re.sub('\D', '', f)))
labels = glob.glob('datasets/labels/*.txt')
labels.sort(key=lambda f: int(re.sub('\D', '', f)))

#move 20 images and labels to test folder, the rest split 90-10 for train and val
test_images = random.sample(images, 20)
test_labels = [i.replace('images', 'labels') for i in test_images]
test_labels = [i.replace('jpg', 'txt') for i in test_labels]

for i in test_images:
    shutil.move(i, 'datasets/images/test')
for i in test_labels:
    shutil.move(i, 'datasets/labels/test')

train_images = [i for i in images if i not in test_images]
train_labels = [i for i in labels if i not in test_labels]

val_images = random.sample(train_images, 12)
val_labels = [i.replace('images', 'labels') for i in val_images]
val_labels = [i.replace('jpg', 'txt') for i in val_labels]

for i in val_images:
    shutil.move(i, 'datasets/images/val')
for i in val_labels:
    shutil.move(i, 'datasets/labels/val')

train_images = [i for i in train_images if i not in val_images]
train_labels = [i for i in train_labels if i not in val_labels]

for i in train_images:
    shutil.move(i, 'datasets/images/train')
for i in train_labels:
    shutil.move(i, 'datasets/labels/train')

#check if images and labels are moved
images = glob.glob('datasets/images/test/*.jpg')
images.sort(key=lambda f: int(re.sub('\D', '', f)))
labels = glob.glob('datasets/lables/test/*.txt')
labels.sort(key=lambda f: int(re.sub('\D', '', f)))

print(f'test: {len(images)}, {len(labels)}')

images = glob.glob('datasets/images/val/*.jpg')
images.sort(key=lambda f: int(re.sub('\D', '', f)))
labels = glob.glob('datasets/lables/val/*.txt')
labels.sort(key=lambda f: int(re.sub('\D', '', f)))

print(f'val: {len(images)}, {len(labels)}')

images = glob.glob('datasets/images/train/*.jpg')
images.sort(key=lambda f: int(re.sub('\D', '', f)))
labels = glob.glob('datasets/labels/train/*.txt')
labels.sort(key=lambda f: int(re.sub('\D', '', f)))

print(f'train: {len(images)}, {len(labels)}')
