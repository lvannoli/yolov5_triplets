import os

folder_path = "E:/cnn_picture/flex_circles"
file_extension = ".jpg"

# Get the list of files in the folder
files = os.listdir(folder_path)

# Sort the files alphabetically
files.sort()
print(files)

# Set the initial number
number = 1

# Iterate through the files and rename them

for filename in files:
    if filename.endswith(file_extension):
        new_filename = f"{number}{file_extension}"
        file_path = os.path.join(folder_path, filename)
        new_file_path = os.path.join(folder_path, new_filename)
        os.rename(file_path, new_file_path)
        number += 1