import os

folder_path = '/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/yolov5_triplets/datasets/labels/test'  # Replace with the path to your folder

for filename in os.listdir(folder_path):
    if filename.endswith('.txt') and len(filename) > 10:  # Process only text files with more than 10 characters in the name
        file_path = os.path.join(folder_path, filename)
        with open(file_path, 'r') as file:
            lines = file.readlines()

        with open(file_path, 'w') as file:
            for line in lines:
                if line.strip().startswith('0'):
                    line = '1' + line[1:]
                file.write(line)