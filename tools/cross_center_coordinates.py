import os
import numpy as np
import matplotlib.pyplot as plt
import glob
import re

# Load the image
input_folder = "/Users/leonardovannoli/Documents/PostDoc/cnn_triplets_pick-and-place/cnn_triplets/data/figures/"
output_folder = "/Users/leonardovannoli/Documents/PostDoc/cnn_triplets_pick-and-place/cnn_triplets/data/labels/"
img_name = ''
cross_output_name = ''
center_output_name = ''
mask_output_name = ''
event = None
img = None
img_copy = None

def pixel_selection():
    # Display the image
    fig, ax = plt.subplots(figsize=(8,8))
    ax.imshow(img)
    plt.title(img_name.split('/')[-1][:-4])
    # Define a global variable to keep track of whether the mouse is being dragged or not
    global is_dragging
    is_dragging = False
    # Define a callback function for when the mouse is clicked on the image
    def on_click(event):
        global is_dragging
        if event.inaxes is not None and event.button == 1:
            # Get the pixel coordinates of the click in the zoomed-in view
            x = int(round(event.xdata))
            y = int(round(event.ydata))
            # Highlight the clicked pixel in the original image
            plt.plot(x,y,'ks',markersize=3)
            #plt.title(img_name.split('/')[-1][:-4])
            plt.draw()
            # Add the pixel coordinates to a numpy array
            if 'pixels' not in globals():
                pixels = np.array([[x], [y]])
                #print(x,y)
            else:
                pixels = globals()['pixels']
                if not np.any(np.all(pixels == np.array([[x], [y]]), axis=0)) or np.any(np.array([[x],[y]])>500) or np.any(np.array([[x],[y]])<0):
                    pixels = np.concatenate((pixels, [[x], [y]]), axis=1)
                    #print(x,y)
                    print(pixels.shape)
            globals()['pixels'] = pixels
            # Set the is_dragging variable to True
            is_dragging = True

    # Connect to the on_motion event to change the color of pixels while dragging the mouse
    def on_motion(event):
        global is_dragging
        if event.inaxes is not None and event.button==1 and is_dragging:
            # Get the pixel coordinates of the current mouse position
            x = int(round(event.xdata))
            y = int(round(event.ydata))
            plt.plot(x,y,'ks',markersize=3)
            plt.draw()
            # Set the color of the pixel to red
            # Add the pixel coordinates to the numpy array if they are not already stored
            if 'pixels' not in globals():
                pixels = np.array([[x], [y]])
                #print(x,y)
            else:
                pixels = globals()['pixels']
                if not np.any(np.all(pixels == np.array([[x], [y]]), axis=0)) or np.any(np.array([[x],[y]])>500) or np.any(np.array([[x],[y]])<0):
                    pixels = np.concatenate((pixels, [[x], [y]]), axis=1)
                    #print(x,y)
                    print(pixels.shape)
            globals()['pixels'] = pixels
            
    # Connect to the on_release event to disconnect from the on_motion event
    def on_release(event):
        global is_dragging
        if event.button == 1:
            # Set the is_dragging variable to False
            is_dragging = False
                            
    # Connect the callback functions to the mouse events
    cid = fig.canvas.mpl_connect('button_press_event', on_click)
    mid = fig.canvas.mpl_connect('motion_notify_event', on_motion)
    rid = fig.canvas.mpl_connect('button_release_event', on_release)

    # Define a callback function for when the scroll wheel is used
    orig_xlim = ax.get_xlim()
    orig_ylim = ax.get_ylim()
    def on_scroll(event):
        if event.inaxes is not None:
            # Get the current x and y limits of the plot
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            # Calculate the new limits based on the scroll direction
            factor = 1.5 if event.button == 'up' else 1/1.5
            xcenter, ycenter = event.xdata, event.ydata  # position of the mouse cursor
            xwidth = xlim[1] - xlim[0]
            ywidth = ylim[1] - ylim[0]
            new_xlim = [xcenter - xwidth/(2*factor), xcenter + xwidth/(2*factor)]
            new_ylim = [ycenter - ywidth/(2*factor), ycenter + ywidth/(2*factor)]
            # Set the new limits
            ax.set_xlim(new_xlim)
            ax.set_ylim(new_ylim)
            plt.draw()
    # Connect the callback function to the scroll event
    cid2 = fig.canvas.mpl_connect('scroll_event', on_scroll)
    # Show the plot
    plt.show()
    # Save the pixel coordinates as a numpy array
    if 'pixels' in globals():
        np.save(cross_output_name, pixels)
        print(f"numpy array salvato in {cross_output_name}.npy")

def final_check(cords, color = 'k'):
    image = np.zeros((500, 500, 3))
    image = image.astype(np.uint8)
    x=cords[0].astype(int)
    y=cords[1].astype(int)
    for i in range(len(x)):
        image[y[i], x[i]] = [255, 255, 255]
    _, ax = plt.subplots(figsize=(8,8))
    ax.imshow(img)
    ax.imshow(image, alpha=0.5)
    plt.title(img_name.split('/')[-1][:-4])
    #plt.plot(x,y,f'{color}s',markersize=2)
    np.save(mask_output_name, image)
    plt.draw()
    plt.show()

def cross_center_finder(cross):
    x_coords = cross[0]
    #print(x_coords)
    y_coords = cross[1]
    #print(y_coords)
    x_center = int(np.mean(x_coords))
    y_center = int(np.mean(y_coords))
    print(f"the center of the cross = {x_center, y_center}")
    cross_center = np.array([[x_center], [y_center]])
    final_check(cross_center, color = 'r')
    #cross = np.concatenate((cross, [[x_center], [y_center]]), axis=1)
    np.save(center_output_name, cross_center)

if __name__ == "__main__":
    img_names = glob.glob(os.path.join(input_folder, "*175*")) #*175*
    print(len(img_names))
    img_names.sort()
    for i,img_name in enumerate(img_names):
        print('----------------------------------')
        try:
            globals()['pixels'] = np.empty((2, 0))
        except:
            print("no pixels to delete")
        event = None
        #char_img_name = ''.join((z for z in img_name.split('/')[-1][:-4] if not z.isdigit()))
        numb_img_name = str(''.join((z for z in img_name.split('/')[-1][:-4] if z.isdigit())))
        #output_name = output_folder+img_name.split('/')[-1][:-4]+"_coords"
        #center_output_name = output_folder+'labels/'+char_img_name+"cross_center_coords_"+numb_img_name
        #center_output_name = output_folder+'labels'+numb_img_name
        #print(f"{center_output_name=}.npy")
        #cross_output_name = output_folder+'crosses/'+char_img_name+"cross_coords_"+numb_img_name
        cross_output_name = output_folder+'crosses/'+numb_img_name
        print(f"{cross_output_name=}.npy")    
        mask_output_name = output_folder+numb_img_name
        print(f"{mask_output_name=}.npy")
        img_path = img_name
        img = plt.imread(img_path)
        img_copy = np.copy(img)
        numpy_path = center_output_name+".npy"
        try: 
            cords = np.load(center_output_name+".npy")
            for i in range(cords.shape[1]):
                x, y = cords[:,i]
                img_copy[y,x] = [1, 0, 0] 
            fig, ax = plt.subplots(figsize=(8,8))
            ax.imshow(img_copy)
            plt.title(img_name.split('/')[-1][:-4])
            plt.show()
            cont = input("Continue with the pixel selection? (y/n)")
        except:
            print("No numpy array found. Starting pixel selection")
            cont = "y"
        if cont == "y":
            pixel_selection()
            #try:
            print(f"{cross_output_name=}.npy")
            final_check(np.load(f"{cross_output_name}.npy"))
            print("mask saved")
                #cross_center_finder(np.load(cross_output_name+".npy"))
            #except:
            #    print("No numpy array found")
            
    exit(0)
