import torch
import torchvision.transforms as T
import cv2

# Load the trained model
model = torch.hub.load('ultralytics/yolov5', 'custom', path='/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/yolov5_triplets/yolov5/runs/train/flex_circle_170epochs/weights/best.pt')

# Set the model to evaluation mode
model.eval()

# Define the transformations for the input images
transform = T.Compose([
    T.ToPILImage(),
    T.ToTensor()
])

# Load and preprocess the test image
image = cv2.imread('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/yolov5_triplets/datasets/flex_circ/test/images/52_jpg.rf.699b85af721fe447d639ac314cb2b595.jpg')
input_image = transform(image).unsqueeze(0)

# Perform inference
with torch.no_grad():
    detections = model(input_image)