# yolov5_triplets


## How to use:
### Training:
Look at the example in tutorial.ipynb
For the triplets gluing task a script called cross_training.py has been used.

### To produce the labels
A very usefull tool is [roboflow](https://app.roboflow.com/). Easy to use, faster than other (or custom) tool to create the labels.

### Test
Look at cross_test.py where Yolov5 is actually used to make object recognition in new images. 
